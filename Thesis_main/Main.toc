\contentsline {chapter}{Zusammenfassung}{10}
\contentsline {chapter}{\numberline {1}Einleitung}{12}
\contentsline {section}{\numberline {1.1}Marktentwicklung}{12}
\contentsline {section}{\numberline {1.2}\acl {lfp} Eigenschaften}{13}
\contentsline {section}{\numberline {1.3}Aufgaben und Ziele dieser Arbeit}{15}
\contentsline {chapter}{\numberline {2}Theoretische Grundlagen}{16}
\contentsline {section}{\numberline {2.1}\acf {bms} Grundidee}{16}
\contentsline {subsection}{\numberline {2.1.1}\acf {cccv}}{17}
\contentsline {subsection}{\numberline {2.1.2}Regler}{18}
\contentsline {subsection}{\numberline {2.1.3}Messsystem}{19}
\contentsline {subsection}{\numberline {2.1.4}Monitor}{20}
\contentsline {subsection}{\numberline {2.1.5}Balancer}{21}
\contentsline {subsection}{\numberline {2.1.6}Protektor}{22}
\contentsline {subsection}{\numberline {2.1.7}Funktionalit\IeC {\"a}tsvergleich}{22}
\contentsline {section}{\numberline {2.2}Technologie}{22}
\contentsline {subsection}{\numberline {2.2.1}Analog \acl {bms}}{23}
\contentsline {subsection}{\numberline {2.2.2}Digital \acl {bms}}{23}
\contentsline {section}{\numberline {2.3}Balancing}{24}
\contentsline {subsection}{\numberline {2.3.1}Aktives Balancing}{25}
\contentsline {subsubsection}{Ladungspumpe}{25}
\contentsline {subsubsection}{Ladungs\IeC {\"u}bertragung mittels Induktivit\IeC {\"a}t}{26}
\contentsline {subsubsection}{Ladungs\IeC {\"u}bertragung mittels Transformator}{27}
\contentsline {subsection}{\numberline {2.3.2}Passives Balancing}{28}
\contentsline {subsection}{\numberline {2.3.3}Balancing Zusammenfassung}{29}
\contentsline {subsubsection}{Top Balancing}{30}
\contentsline {subsubsection}{Bottom Balancing}{30}
\contentsline {subsubsection}{Continuous Balancing}{30}
\contentsline {section}{\numberline {2.4}Schutz}{30}
\contentsline {chapter}{\numberline {3}Stand der Technik}{32}
\contentsline {section}{\numberline {3.1}Zentrales \acs {bms}}{33}
\contentsline {section}{\numberline {3.2}Modulares \acs {bms}}{34}
\contentsline {section}{\numberline {3.3}Master - Slave oder Semi-Distributed}{35}
\contentsline {section}{\numberline {3.4}Verteiltes \acs {bms} oder Distributed \acs {bms}}{36}
\contentsline {section}{\numberline {3.5}Vergleich von aktueller \acf {bms} Konzepte}{37}
\contentsline {section}{\numberline {3.6}Kommunikation f\IeC {\"u}r die \acf {bms} Komponente}{37}
\contentsline {chapter}{\numberline {4}Implementierung}{38}
\contentsline {section}{\numberline {4.1}\acl {bms} Entwicklung}{39}
\contentsline {section}{\numberline {4.2}Zielhardware und eingesetzte Werkzeuge}{41}
\contentsline {subsection}{\numberline {4.2.1}\acf {bcu}}{42}
\contentsline {subsection}{\numberline {4.2.2}\acf {pcu}}{45}
\contentsline {subsection}{\numberline {4.2.3}Sicherheitssystem}{46}
\contentsline {section}{\numberline {4.3}Kommunikation zwischen \ac {bms}-Komponenten}{47}
\contentsline {subsection}{\numberline {4.3.1}\ac {isospi} Kommunikationsschnittstelle}{48}
\contentsline {subsection}{\numberline {4.3.2}\ac {can} Kommunikationsschnittstelle}{49}
\contentsline {subsection}{\numberline {4.3.3}Daten\IeC {\"u}bertragung zwischen \ac {bcu} und \ac {pcu}}{49}
\contentsline {chapter}{\numberline {5}Funktionsbeschreibung}{51}
\contentsline {section}{\numberline {5.1}Spannungsmessung}{51}
\contentsline {section}{\numberline {5.2}Strommessung}{54}
\contentsline {section}{\numberline {5.3}Temperaturmessung}{55}
\contentsline {section}{\numberline {5.4}Balancing}{57}
\contentsline {subsection}{\numberline {5.4.1}Balancing Feedback}{59}
\contentsline {subsection}{\numberline {5.4.2}Temperatur Management}{60}
\contentsline {section}{\numberline {5.5}Sicherheitssystem}{62}
\contentsline {subsection}{\numberline {5.5.1}Aufbau und Implementierung des Sicherheitssystems}{62}
\contentsline {chapter}{\numberline {6}Fazit}{66}
