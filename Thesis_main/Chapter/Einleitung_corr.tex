\chapter{Einleitung}

Das Thema „Energiewende“ hat seit seiner Einführung 2011, als Folge von Fukushima, die Energiepolitik in Deutschland verändert und wird diese auch noch weiterhin verändern. Ziel ist es, eine Abkehr von der Atomenergie hin zu möglichst CO\textsubscript{2} neutralen erneuerbaren Energien zu schaffen. Batteriespeicher spielen dabei eine besondere Rolle. Speicher können grundsätzlich Energie aufnehmen und zu einem späteren Zeitpunkt wieder abgeben. Im Rahmen der Energiewende haben Speicher jedoch eine besondere Rolle, da sie in der Lage sind das Problem der fluktuierenden Einspeisung der Erneuerbaren Energien zu lösen. Energiespeicherung ist auf Kurzzeitspeicher und Langzeitspeicher verteilt. Kurzzeitspeicher liefern die Energie im Sekundenbereich, um die Stromversorgung sicherzustellen, Langzeitspeicher sind zur Lastverschiebung im Mittel- bis Hochspannungsbereich im Einsatz, der Speicherbedarf im Vergleich zu Kurzzeitspeicher ist wesentlich größer.

\section{Marktentwicklung}

Der elektrochemische Speicher hat sich in den letzten Jahren im Bereich der Autoindustrie als auch Energiespeicherung durchgesetzt. Aufgrund der steigenden Nachfrage an elektrochemischen Speichern und die dadurch vorangetriebene Technologieentwicklung, sind Zellen auf Lithium-Ionen Basis auf den Markt gebracht worden (siehe \cite[Kap. 2.2]{2013RheinerKorthauer}). Vorteile von Lithium-Ionen Zellen und der daraus abgeleiteten Systeme sind eine große spezifische Energie, große spezifische Leistung, hoher Wirkungsgrad beim Laden und Entladen und eine geringe Selbstentladung\cite[Kap. 2.3]{Weicker2013}. Lithium Ionen Zellen besitzen große Vorteile durch ihre Vielzahl an Einsatzgebieten. Die Zellen werden für Speicherung von elektrischer Energie eingesetzt. Die drei Einsatzgebiete von stationären Energiespeichern sind:

\begin{itemize}
	\item Netzstabilisierung als Primär- oder Sekundärregelleistung
	\item Eigenverbrauchserhöhung (bzw. Autarkie) typischerweise in Kombination mit einer Photovoltaikanlage
	\item Glätten von Lastspitzen (Peak Shaving)
\end{itemize} 

Die Lithium Ionen Zellen besitzen jedoch den Nachteil, dass sie gegenüber missbräuchlicher Behandlung wie Überladung, Tiefentladung, Übertemperatur und übermäßigem Strom weniger tolerant sind. Unter diesen Bedingungen verlieren die Zellen Kapazität, zudem steigt der Innenwiderstand, was zu stärkerer Wärmeentwicklung führt. Im kritischsten Fall kann das sogar zur Explosion führen, was eine unmittelbare Gefahr für die Umwelt bedeutet.

Um die Zellen vor Missbrauch zu schützen, die Lebensdauer zu verlängern und frühzeitige Degradierung der Zellen zu erkennen, muss ein \acl{bms} eingesetzt werden. Das heißt, dass für diese Zwecke zusätzliche Elektronik benötigt wird, welche die vollständige Zellenüberwachung und Ansteuerung sicherstellt und vor allen möglichen Fehlerfällen schützt.

Da der Wettbewerb auf dem Markt für \ac{bms} Systeme groß ist, kam die Idee auf, eine flexible, programmierbare \acl{bms} Hardware zu schaffen, die perfekt auf die Eigenschaften der \acf{lfp} angepasst sind. Die Anpassung soll zudem in der Software stetig optimierbar sein.

\section{\acl{lfp} Eigenschaften}

\label{lipo_anforderungen}
Lithiumbatterien finden Anwendung in vielen portablen Geräten und auch in großen elektrischen Energiespeicheranlagen  (\cite[Kap. 3]{PeterKurzweil2018}). Die \acl{lfp} Zelle besitzt eine Leerlaufspannung von 3,3V und eine spezifische Energie bzw. Energiedichte von $544\frac{Wh}{Kg}$ (siehe \cite[Kap. 4,5]{2013RheinerKorthauer}). Um die Zellen möglich sicher zu machen, wurden die Anforderungen an die Entwicklung von \ac{lfp} Zellen erhöht. Die moderne Lithium-Ionen Batterie soll folgenden Anforderungen entsprechen siehe auch \cite[Kap. 3.2]{PeterKurzweil2018}.
\begin{enumerate}
	\item leichter und kompakter Aufbau, ohne schweres Metallgehäuse
	\item frei von giftigen Metallen
	\item verlässlich und inhärent sicher
	\item in verschiedenen Größen und Ausführungen verfügbar
	\item Langlebigkeit mit einer Zyklendauer mehrerer tausend Zyklen
	\item preiswert
	\item großer Temperatur- Arbeitsbereich
	\item niedrige Selbstentladung $\leq 1\%$ im Jahr
	\item hohe Leistungsdichte und Entladespannung
\end{enumerate}

Die LiFePo4 Zellen unterscheiden sich qualitativ je nach Hersteller, aber die Basiseigenschaften dafür sind

\begin{itemize}
	\item Leerlaufspannung
	\item Kapazität
	\item Innenwiderstand
	\item Maximal zulässiger Ladestrom
	\item Maximal zulässiger Entladestrom
	\item Maximal zulässige Betriebstemperatur
\end{itemize}

Die meisten Parameter sind aufgrund chemischer Zusammensetzung und Bauart konstant. Der Innenwiderstand der Zellen kann werksseitig verbessert werden und ist entscheidend bei der Batterieauswahl.
Der Spannungsabfall wird vom Innenwiderstand unter Last, der Zellerwärmung und Selbstentladung beeinflusst siehe \cite[Kap. 2.7]{2013RheinerKorthauer}.

Aufgrund der Tatsache, dass die Zellspannung vom Innenwiderstand abhängig ist, ist es möglich, mittels Zellspannung- und Strommessung den Innenwiderstand zu bestimmen.

\begin{equation}
	\label{eq. innenwiderstand von die Zelle}
	R_{Zelle} = \frac{U_{Zelle_{Leerlauf}}-U_{Zelle}}{I_{Zelle}}
\end{equation}

Mithilfe von Gleichung \ref{eq. innenwiderstand von die Zelle} lässt sich der Zellenwiderstand berechnen und damit der Alterungsstand oder State of Health (SOH) der Zelle bestimmen. Über die Zeit nimmt die verfügbare Kapazität in der Zelle ab, außerdem kommt es zu einem Anstieg des Innenwiderstands, was zu ungleichen Ladeständen in der Batterie führen kann. Dies beeinflusst das Ladeverhalten und damit die Lebensdauer der Zellen (\cite[Kap. 2.7]{2013RheinerKorthauer}).

Alterungsstand oder Güte ist ein wichtiges Maß der Zelle, da ein Defekt einer einzigen Zelle zum Ausfall der Gesamtbatterie führen kann. D.h., die Gesamtbatterie ist nur so gut wie deren schlechteste Zelle.

Batterieausfälle können durch Überladung, Tiefentladung, eine zu niedrige oder zu hohe Umgebungstemperatur oder schlechte chemische Eigenschaften auftreten. Die wesentliche negative Beeinflussung ist jedoch, dass durch Produktionstoleranzen und Alterung die Zellen abweichende Kapazität aufweisen (siehe \cite[Kap. 2.7]{2013RheinerKorthauer}) %\cite[]{Symposium2015}.

Damit die Zelle lange Zeit möglichst unbeschädigt bleibt, muss ein System aufgebaut werden, welches die Zellenparameter in ihren optimalen Grenzen hält. Dafür müssen Spannung, Temperatur und Lade- bzw. Entladestrom ständig überwacht werden. Aufgrund der erhaltenen Werte, kann eine Alterung der Zellen frühzeitig erkannt werden.

\section{Aufgaben und Ziele dieser Arbeit}

Im Rahmen der Entwicklung eines neuen Batteriespeichers ist es nötig, die einzelnen Batteriespannungen und Batterietemperaturen zu messen. So kann garantiert werden, dass die Batterie immer in einem zulässig sicheren Arbeitsbereich betrieben wird. Würde man die Batterien ohne diese Überwachung betreiben, könnte beispielsweise eine volle Batterie die überschüssige Energie beim Laden nicht mehr aufnehmen. Diese überschüssige Energie wird dann in Wärme umgewandelt, was zu Schäden an der Batterie und im schlimmsten Fall zu einem thermischen Durchgehen der Zelle führen kann.

Der Schwerpunkt der Arbeit liegt im Entwurf des Schaltplans für die folgenden BMS - Komponenten: Die \acf{pcu}, das Sicherheitssystem sowie die Vorbereitung der \acf{bcu}. Dazu gehört die Wahl von notwendigen Komponenten für die einzelnen Systementwürfe des PCU, BCU und des Sicherheitssystems.  Nach dem Schaltplanentwurf soll ein Prototyp der \ac{pcu} aufgebaut werden und die grundlegenden Funktion im Batterie-Pack überprüft werden. Anschließend soll ein kleiner Demonstrationsstand mit Batterie-Packs erstellt werden. Mithilfe dieses Demonstrationsstand wird eine erste Software-Implementierung des Treibers zur Überwachung der Zellspannungen und Temperaturen entwickelt. Diese Software hat die Aufgaben die Messdaten aufzubereiten und für Entwicklungszwecke auswertbar zu machen. 






