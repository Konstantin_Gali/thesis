\section{Balancing}
	\label{kap. Balancing_Th}
	Das Balancing ermöglicht eine gleichmäßige elektrische Ladungsverteilung, ohne die voll geladenen Zellen zu überladen. Um die LFP - Zellen in einer Batterie auf dem gleichen \ac{soc} zu halten, müssen die in Reihe verschalteten Zellen ausbalanciert werden. Das Balancing wird anhand eines Beispiels aus Abbildung \ref{abb:Balancing-Prozess} erklärt. Nach \ref{abb:Balancing-Prozess}a besitzen die Zellen aufgrund herstellungsspezifischer Toleranzen alle unterschiedliche Kapazitäten sowie Ladestand. Beim Balancing wird Zelle 5 daher nicht geladen und als Zelle mit 100\% SOC als Referenz geführt. Alle anderen Zellen müssen sich an dieses Ladungsniveau anpassen.
	Die so gewählte Methode der Balancierung nennt man Top - Balancing, da man die 100\% geladene Zelle referenziert (s. Abbildung: \ref{abb:Balancing-Prozess}b siehe Kap. \ref{kap: top balancing} und auch   \cite[Kap.: 3.2.3]{Andrea2010}.
	
	Um die Batteriekapazität möglichst effizient auszunutzen, werden die Zellen mit unterschiedlichen Strömen geladen, dazu siehe Kap.\ref{kap: aktiver balancing} und Kap. \ref{kap: Passiver Balancing}. 
	
	\begin{figure}[h]
		\centering
		\includegraphics[scale=1]{Bilder/Theorie/Zellen_Balancing.pdf}
		\caption{Balancing Prozess: (a) nicht ausbalancierte Batterie (b) Top ausbalancierte Batterie. Entnommen aus \cite[Abb. 3.11]{Andrea2010}  }
		\label{abb:Balancing-Prozess}
	\end{figure}
	
	Ein Balancing sichert das Batteriesystem vor Tiefentlade- und Überladefehlern. Eine Implementierung der Lade-Balancing Strategie ist notwendig, um die Batterieunterschiede, die je nach Hersteller und Herstellung unterschiedlich ausfallen, zu berücksichtigen \cite[Kap. 14]{Weicker2013}.
	 
	Im Grunde genommen kann ein Balancing  auf zwei Arten realisiert werden: 
	
	Aktiv: Energie wird auf übrige Zellen verteilt 
	
	Passiv: Energie wird über einen Leistungswiderstand in Wärme umgewandelt \cite{Andrea2010}.
	
	\newpage
	\subsection{Aktives Balancing}
	\label{kap: aktiver balancing}
	
	Ein Aktives Balancing (auch Charge Transfer Balancing genannt) kann auf drei unterschiedliche Arten aufgebaut werden.
	 
	\subsubsection{Ladungspumpe}
		
		\label{kap: ladungspumpe}
		Bei der Ladungspumpe wird ein Kondensator so genutzt, dass die Zelle den Kondensator lädt, danach wird der geladene Kondensator von der Zelle weggeschaltet und zu einem externen Element geschaltet, welches die gespeicherte elektrische Energie aufnimmt. Die externe Komponente kann ein Leistungswiderstand, eine Messeinheit oder eine andere Zelle mit einem niedrigeren Spannungsniveau sein. Die schematische Darstellung in \ref{abb:Ladungspumpe} zeigt das Funktionsprinzip einer Ladungspumpe. 
		
		\begin{figure}[h]
			\centering
			\includegraphics[scale=1]{Bilder/Theorie/Ladungspumpe.pdf}
			\caption{Schematische Verschaltung der Ladungspumpe. Entnommen aus \cite[Abb. 14.2]{Weicker2013}  }
			\label{abb:Ladungspumpe}
		\end{figure}
		
		Die Funktionsschritte für die Ladungspumpe aus Abbildung \ref{abb:Ladungspumpe} sind folgende:
		\begin{enumerate}
			\item S1 und S2 sind ZU, S3 und S4 sind OFFEN
			\item Kondensator C1 wird geladen
			\item S1 und S2 sind OFFEN, S3 und S4 sind OFFEN
			\item S1 und S2 sind OFFEN, S3 und S4 sind ZU
			\item Kondensator C1 wird entladen
			\item S1 und S2 sind OFFEN, S3 und S4 sind OFFEN
		\end{enumerate}
		Die Schritte von 1 bis 6 wiederholen sich immer wieder, so wird die Ladung von $U1$ zu $R_L$ übertragen. Für diese Ausführung kann anstatt eines Schalters ein elektromechanisches Relay, ein Optokoppler oder Transistoren verwendet werden. Ladungspumpen bieten einen wirtschaftlichen Vorteil. Sie sind zumeist sehr viel billiger als Induktivitäten oder DC/DC Konverter. Das System funktioniert nur weniger effektiv, wenn das $\Delta U$ zwischen zwei ausbalancierenden Zellen zu klein ist. Abhilfe würden Verbindungen zwischen jedem Kondensator und jeder Zelle schaffen. Dies allerdings erfordert eine große Schaltmatrix, die komplex zu implementieren und kostspielig ist \cite[Kap. 14.3.1]{Weicker2013}.
		
	\subsubsection{Ladungsübertragung mittels Induktivität}
		Die Ladungsübertragung durch eine Ladungspumpe aus Kap. \ref{kap: ladungspumpe} ist nur denkbar, wenn das $\Delta U$ zwischen zwei Zellen ausreichend groß ist. Für andere Fälle ist es sinnvoll, eine Induktivität zur Ladeübertragung zu verwenden wie in Abbildung \ref{abb:Ladungspumpe_ind}a und b. Eine induktive Ladeübertragung findet ebenfalls Anwendung, wenn der Ladestand aller Zellen ziemlich nah beieinander liegt, diese Methode kann die Leistung des Systems erheblich verbessern \cite[Kap.:14.3.2]{Weicker2013}. 
		
		Die Technologie ist in zwei möglichen Ausführungen realisierbar. Erste Option ist in Abbildung: \ref{abb:Ladungspumpe_ind}a aufgezeigt. Sie ermöglicht die Ladeübertragung von der $Zelle_{n+1}$ zu der $Zelle_{n}$, also unidirektional. Bei der Implementierung von zusätzlichen Schaltern und Dioden, so wie in Abbildung \ref{abb:Ladungspumpe_ind}b, kann das System den Ladetransport bidirektional realisieren. Für eine bidirektionale Ladeübertragung werden mehrere elektronische Elemente benötigt, was zu zusätzlichen Produktionskosten führen kann.
		
		\begin{figure}[h]
			\centering
			\includegraphics[scale=1]{Bilder/Theorie/Ladungspumpe_ind.pdf}
			\caption{(a) Ladeübertragungsschaltung unidirektional. (b) Ladeübertragungsschaltung bidirektional. Entnommen aus \cite[Abb. 14.3 und 14.4]{Weicker2013}}
			\label{abb:Ladungspumpe_ind}
		\end{figure}
		
		Die Funktionsschritte der Ladeübertragung sind folgende:
		
		\begin{enumerate}
			\item S1 ist zu, Strom von der $ZELLE_{n+1}$ fließt über die Induktivität L1. Das führt zu einer Selbstinduktion was erzeugt ein Magnetfeld in L1;
			\item S1 ist offen, der Strom fließt aus L1 über Diode D1 in die $ZELLE_{n}$.
		\end{enumerate}
	
		Mit dem weiter treibenden Strom aus L1 lädt sich die $ZELLE_{n}$. Nach gleichem Muster wird die Ladeübertragung auch bidirektional realisiert, der Schalt- und Diodenaufwand ist hier nur höher. Die dazugehörige Schritte können wie folgt beschrieben werden:
		\begin{itemize}
			\item Ladungsübertragung von der  $ZELLE_{n+1}$ zu $ZELLE_{n}$
			\begin{enumerate}
				\item S1 ist zu, Strom von der $ZELLE_{n+1}$ fließt über die Induktivität L1. Das führt zu einer Selbstinduktion, die ein Magnetfeld in L1 erzeugt;
				\item S1 ist offen, der Strom fließt aus L1 über Diode D1 in die $ZELLE_{n}$.
			\end{enumerate}
		\end{itemize}
		\begin{itemize}
			\item Ladungsübertragung von der  $ZELLE_{n}$ zu $ZELLE_{n+1}$
			\begin{enumerate}
				\item S3 ist zu, Strom von der $ZELLE_{n}$ fließt über die Induktivität L1. Das führt zu einer Selbstinduktion, die ein Magnetfeld in L1 erzeugt;
				\item S3 ist offen, der Strom fließt aus L1 über Diode D3 in die $ZELLE_{n}$.
			\end{enumerate}
		\end{itemize}
		
		
		Der große Vorteil ist, dass diese Methode nicht vom Spannungsunterschied zwischen den Zellen abhängig ist. Die Spannung in  Zelle  $ZELLE_{n+1}$ muss nicht größer als in $ZELLE_{n}$ sein.
		
		Um den Wirkungsgrad zu verbessern, kann eine Schottky Diode mit kleinst möglicher Durchlassspannung ausgewählt werden. So können die Verluste durch die Diode reduziert werden. Der Balancing Strom ist nur von der Induktivität und den ausgewählten Bauteilen limitiert.
		
		Das System ist für eine Ladeübertragung zwischen nebenstehenden Zellen konzipiert. Bei der Ladeübertragung über mehrere Zellen hinweg, wird der Wirkungsgrad wesentlich geringer \cite[Kap.: 14.3.2]{Weicker2013}.
		
	\subsubsection{Ladungsübertragung mittels Transformator}
	
		Ein Balancing System mit Induktivität ermöglicht eine Ladeübertragung unabhängig von der Zellspannung aber nur zwischen benachbarten Zellen. Ein Erweiterung ist das Ersetzen der Induktivität durch einen Transformator. Ein Transformator in einem DC/DC Konverter System kann die Energie zwischen einer Zelle und einer anderen Spannungsquelle austauschen/transportieren \cite[Kap.: 14.3.3]{Weicker2013}.
		
		Einer der möglichen Ausführungen dafür ist ein Transformator Balancing Konzept \cite[Kap.: 14.4]{Weicker2013}. Das Schema dafür ist in Abbildung \ref{abb:Aktiverbalancing}a abgebildet und zeigt eine bidirektionale Energieübertragung. Bidirektionale Ausführungen ermöglichen die Ladeübertragung zwischen Zelle-Batterie, Batterie-Zelle \cite[Kap.: 3.2.3.4]{Andrea2010}. Das dargestellte System (vgl. Abbildung \ref{abb:Aktiverbalancing}a) kann auch unidirektional realisiert werden.
		
		Die Ladeübertragung ermöglicht eine Umverteilung (Redistribution). Die Zellströme der Zellen teilen sich so auf, dass der \ac{soc} einer einzelnen Zelle dem \ac{soc} der gesamten Batterie entspricht, siehe  \ref{abb:Aktiverbalancing}b. Durch diese Technik entspricht der Batteriestrom nicht dem Zellenstrom, das heißt die Zelle mit der kleinsten Kapazität liefert weniger Strom als die Zelle mit größten Kapazität. Die dazu gehörige Abbildung \ref{abb:Aktiverbalancing}b stellt einen möglichen Stromfluss dar, wenn eine Zelle mit 120A entladen wurde und eine andere mit 80A \cite[Kap.: 3.2.4]{Andrea2010}.  
		
		\begin{figure}[h]
			\centering
			\includegraphics[scale=1]{Bilder/Theorie/AktiverBalancing_mit_strom.pdf}
			\caption{(a) Bidirektionale Ladeübertagungssystem mit Transformator, welcher die Energieübertragung zwischen Zelle und Batterie realisiert. (b) Entladung der Batterie mit unterschiedlichen Strömen durch aktives Balancing \cite[Kap. 3.2.4]{Andrea2010}}
			\label{abb:Aktiverbalancing}
		\end{figure}

	 
	\subsection{Passives Balancing}
		\label{kap: Passiver Balancing}
	
		Das passive Balancing ist die kostengünstigere Balancing - Variante, die die überschüssige Energie der zu balancierenden Zelle in Wärme umwandelt. Die Energieumwandlung erfolgt mittels eines Leistungswiderstands, der mit der Zelle in Reihe verschaltet ist. Die Schalter müssen nur für eine Zelle jeweils implementiert werden, was den Aufbau sehr vereinfacht, siehe \cite[Kap.: 14.4]{Weicker2013}. Der Schalter verbindet die Zelle und den Leistungswiderstand miteinander. Dieser beeinflusst den Stromfluss der Zelle wie in Abbildung \ref{abb:Passiverbalancing} abgebildet.
		
		\begin{figure}[h]
			\centering
			\includegraphics[scale=1]{Bilder/Theorie/PassiverBalancing.pdf}
			\caption{Typische passive Balancing Schaltung. Entnommen aus \cite[Abb. 14,6]{Weicker2013}}
			\label{abb:Passiverbalancing}
		\end{figure}
		
		Bei der Nutzung von \ac{smd} Leistungswiderständen während des Balancingvorgangs werden die Widerstände aufgrund ihrer geringen Größe sehr schnell warm. Die Komponenten heizen so auch die Leiterplatte auf. Beim Platinenlayout muss auf diese Komponenten daher besonders geachtet werden.
		
		Die Wärmeabfuhr von den Widerständen sollte extern mit einer Art Kühlung erfolgen. Oft wird die Kühlung allerdings ohne Kühlkörper oder Kühlsystem realisiert, die Wärme wird über Lötstellen durch die Leiterplatte abgeführt \cite[Kap.: 14.4]{Weicker2013}.
		
		Für qualitativ hochwertige Zellen mit gleicher Kapazität und möglichst gleichem Widerstand ist diese Implementierungsart von Vorteil. Sie vereinfacht den Aufbau und reduziert die Kosten \cite[Kap.: 14.4]{Weicker2013}. Allerdings wird die Balancing Leistung irreversibel in Wärme umgewandelt \cite[Kap. 8.2]{Andrea2010}.		
			 
	\subsection{Balancing Zusammenfassung}
	
		Die balancierende Einheit des \ac{bms} gleicht den Ladestand der Zellen auf den gleichen \ac{soc} aus. Die Zellen werden während des Balancing mit Lade- oder Entladeströmen belastet und ändern damit ihren Ladestand. Die beiden Ausführungen des Balancing bieten unterschiedliche Balancingarten mit gleicher Zielsetzung. Passives Balancing ist einfach implementierbar und kann kostengünstig und mit wenigen Elektronikkomponenten aufgebaut werden. Dazu wird die überschüssige Energie über einen Leistungswiderstand dissipiert. Die andere Ausführung beschreibt den Prozess des aktiven Balancing. Dieser beschreibt ein komplexeres System und ist hochpreisiger. Die überschüssige Energie wird dort zwischen den übrigen nicht vollgeladenen Zellen verteilt. Grundsätzlich bestimmt man einen bestimmten Strombereich während der Balancingvorgänge: Für das passive Balancing liegt der Wert bei 10mA bis 1A und für das aktive Balancing zwischen 100mA und 10A.
		
		Die Balancing Strategien sind auf drei mögliche Ausführungen verteilt. Diese sind \glqq top, bottom und continuous\grqq{} Balancing.
		%TODO Balancingstrategien Schreiben
			\subsubsection{Top Balancing}
				\label{kap: top balancing}
				Beim Top Balancing startet der Vorgang, wenn eine Zelle geladen ist oder eine bestimmte Spannungsgrenze erreicht hat. Der Ladestrom im Fall des Top Balancing ist niedriger als der Entladestrom. Die Zelle wird bis auf eine bestimmte Grenze entladen und danach wieder geladen. So pendelt sich das Ladeniveau aller Zellen nach einiger Zeit auf das höchste Spannungsniveau ein \cite[Kap.: 3.2.3]{Andrea2010}.
				
			\subsubsection{Bottom Balancing}
				
				Bei der Bottom Balancing startet der Vorgang, wenn eine Zelle in der Batterie voll entladen ist. Die restliche Zellen werden durch Bottom Balancing auf den gleiche Stand wie die voll entladene Zelle gebracht \cite[Kap.: 3.2.3]{Andrea2010}. 
				
			\subsubsection{Continuous Balancing}
				
				Beim Continuous Balancing balancieren die Zellen die ganze Zeit solange es einen Ladeunterschied zwischen den Zellen gibt. Der kleinste \ac{soc} bleibt als Referenz für das Balancing. Wenn der Referenzwert dieser Zelle überschritten ist, schaltet das Balancing für die angeforderte Zelle ein \cite[Kap.: 3.2.3]{Andrea2010}.
		
	
	
	
	
	
	
	
