//Präsentation Anfang November - Graph graue Verteilung

clc;

dat = readxls('Widerstande Vergleich.xls');
datenmenge = dat(1);
stats = datenmenge.value

y1 = stats(:,1);
t1 = stats(:,2);
y2 = stats(:,3);
t2 = stats(:,4);
y3 = stats(:,5);
t3 = stats(:,6);

//Plotten
scf(4);
clf();
xgrid(0);
plot2d(t1,y1,color("red"));//6
plot2d(t2,y2,color("green")); //7
plot2d(t3,y3,color("blue")); //16

legend("$\text{15 Ohm}$","$\text{10 Ohm}$","$\text{3,3 Ohm}$",2)


// Layout für PNG Designs
f = gcf();
f.figure_size = [1200,1000]; //For PNG Desine
f.axes_size = [1150,950]; //Graph size
f.children.font_size = 5; //
f.children.thickness = 1;

f.children.children(2).children.thickness = 3; //Polyline features
f.children.children(3).children.thickness = 3;
f.children.children(4).children.thickness = 3;

g = gce(); //Legende menu
g.thickness = 3;
g.font_size = 5;

xlabel('$\text{Leisung in W}$', 'fontsize', 6);
ylabel('$\Delta\text{Temperatur in }^\circ\text{C}$', 'fontsize',6);
xs2pdf(4,"Widerstandvergleich.pdf")
