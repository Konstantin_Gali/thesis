//Verarbeitungsprogramm und Plotprogramm für 8 Sensoren
clc;clear; 

//Korrektes Einlesen des csv-files

cellVoltage = [1:0.2:4];
resistorValue = [3.3; 5; 10; 15];

//Normierung Zeitwert in Sekunde(1)/Minute(60)/Stunde(3600)
//Einlesen von y-Werten


for n = 1:1:4
    step = 1;
for x = 1:0.2:4
    y(step,n) = (cellVoltage(step)^2) / resistorValue(n);
    step = step + 1;
end
end

//Graphik - Einstellungen

scf(4); //Nummer des Graphik-Fensters
clf(); //Löschen des Graphikhintergrunds
xgrid(0); //Schwarzes Grid

//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(cellVoltage,y(:,1),color("scilab blue4")); //7
plot2d(cellVoltage,y(:,2),color("orange"));
plot2d(cellVoltage,y(:,3),color("scilab green4"));
plot2d(cellVoltage,y(:,4),color("yellow"));

//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{R = 3.3\Omega}$","$\text{R = 5.1\Omega}$","$\text{R = 10\Omega}$","$\text{R = 15\Omega}$",[2]);

// Titel
//title("$\text{Vergleich unterschiedliche Widerstände}$",'fontsize',6)

// Axes leyout
a = gca();
a.filled = "off"
// Layout für PNG Designs
f = gcf();
f.figure_size = [1366,768]; //For PNG Desine
f.axes_size = [1266,668]; //Graph size
f.children.font_size = 5; //Achsendicke
f.children.thickness = 1; //Grid Dicke
f.background =  -2


//Polyline features (Dicke der Linien)
f.children.children(2).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 3;  //Dicke
g.font_size = 5;  //Schriftgröße
g.visible = "on"
g.line_mode ="off"
g.fill_mode = "on"
g.font_color = -1

//Labels '$' in Latex-Schreibweise
xlabel('$U_{ZELLE}/V$', 'fontsize', 6);
ylabel('$Wärmeleistung/ \text{W}$', 'fontsize',6);
