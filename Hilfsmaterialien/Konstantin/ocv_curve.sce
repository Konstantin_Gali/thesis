//OCV - Kennlinie

clc;clear; 

//Funktionsaufruf
exec('csv_read_conv_functions_soc.sci')

//Korrektes Einlesen des csv-files
fileName = "csv_files/OCV_curve.csv"

[measurementTime, measurementValue]=read_grafana_csv(fileName);

//Normierung Zeitwert in Sekunde(1)/Minute(60)/Stunde(3600)
t1 = measurementTime*10; //1Wire außen
//Einlesen von Außen Sensoren
y101 = measurementValue(:,1);


///////////////////////////////////////////GRAPH 1 /////////////////////////////////////////////////////

//Graphik - Einstellungen
//figure(4);
scf(0); //Nummer des Graphik-Fensterss
figure(0);
//clf(); //Löschen des Graphikhintergrunds
xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t1,y101,1,rect=[0,2800,100,3600]);//2
//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Leerlaufspannung während Ladeprozess}$");

// Layout für TEX Designs
f = gcf();
f.figure_size = [431,360];
f.axes_size = [425,340];
f.children.font_size = 0.5;
f.children.thickness = 0.5;

f.children.children(2).children.thickness = 3; //Polyline features (Dicke der Linien)


g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 1;  //Schriftgröße

//Labels '$' in Latex-Schreibweise

ylabel('$\text{Leerlaufspannung in mV}$', 'fontsize',2);
xlabel('$\text{State of Charge (SOC) in \%}$', 'fontsize',2);


xs2pdf(0,"graphic_output/ocv_curve.pdf");

