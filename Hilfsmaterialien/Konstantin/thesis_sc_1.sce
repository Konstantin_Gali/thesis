//NtTC und 1-Wire Sensoren Ausleser

clc;clear; 

//Funktionsaufruf
exec('csv_read_conv_functions.sci')

//Korrektes Einlesen des csv-files
fileName1 = "csv_files/grafana_data_sc_line1.csv"
fileName2 = "csv_files/grafana_data_sc_line2.csv"

[timeValues1, measurementValue1]=read_grafana_csv(fileName1);
[timeValues2, measurementValue2]=read_grafana_csv(fileName2);

//Normierung Zeitwert in Sekunde(1)/Minute(60)/Stunde(3600)
t1 = timeValues1/60; //NTCs
t2 = timeValues2/60; //1Wire

//Kalibrierwert
umgebung = measurementValue2(:,17);
//Einlesen von 8 y-Werten NTC
y11 = measurementValue1(:,1); 
y12 = measurementValue1(:,2);
y13 = measurementValue1(:,3);
y14 = measurementValue1(:,4);
y15 = measurementValue1(:,5);
y16 = measurementValue1(:,6);
y17 = measurementValue1(:,7);
y18 = measurementValue1(:,8); 
y19 = measurementValue1(:,9);
y110 = measurementValue1(:,10);
y111 = measurementValue1(:,11);
y112 = measurementValue1(:,12);
y113 = measurementValue1(:,13);
y114 = measurementValue1(:,14);
y115 = measurementValue1(:,15); 
y116 = measurementValue1(:,16);
y_balken = ones(666,1)*45;



///////////////////////////////////////////GRAPH 1 /////////////////////////////////////////////////////

//Graphik - Einstellungen
//figure(4);
scf(0); //Nummer des Graphik-Fensters
clf(); //Löschen des Graphikhintergrunds
xgrid(0); //Schwarzes Grid
//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t1,y11);
plot2d(t1,y19);
plot2d(t2,umgebung);
plot2d(t1,y_balken,color("grey"));
plot2d(t1,y12,5);
plot2d(t1,y13,16);
plot2d(t1,y14,1);
plot2d(t1,y15);
plot2d(t1,y16,5);
plot2d(t1,y17,16);
plot2d(t1,y18,1); 
plot2d(t1,y110,5);
plot2d(t1,y111,16);
plot2d(t1,y112,1); 
plot2d(t1,y113);
plot2d(t1,y114,5);
plot2d(t1,y115,16);
plot2d(t1,y116,1);
  

//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Sensor1-8,10-16}$","$\text{Sensor9}$","$\text{Sensor Umgebung}$","$\text{max. zul. Betriebs T.}$",[2])


// Layout für PNG Designs
f = gcf();
f.figure_size = [700,550];
f.axes_size = [680,520];
f.children.font_size = 0.5;
f.children.thickness = 0.5;

f.children.children(16).children.thickness = 3;
f.children.children(16).children.line_style = 8;
f.children.children(17).children.thickness = 1;
f.children.children(17).children.foreground = 4;
f.children.children(17).children.mark_style = 4;
f.children.children(17).children.mark_foreground = 4;
f.children.children(18).children.thickness = 1;
f.children.children(18).children.foreground = 5;
f.children.children(18).children.mark_style = 4;
f.children.children(18).children.mark_foreground = 5;
f.children.children(19).children.thickness = 1;
f.children.children(19).children.foreground = 1;
f.children.children(19).children.mark_style = 4;
f.children.children(19).children.mark_foreground = 1;
f.children.children(5).children.thickness = 1;
f.children.children(5).children.foreground = 1;
f.children.children(5).children.mark_style = 4;
f.children.children(5).children.mark_foreground = 1;
f.children.children(6).children.thickness = 1;
f.children.children(6).children.foreground = 1;
f.children.children(6).children.mark_style = 4;
f.children.children(6).children.mark_foreground = 1;
f.children.children(7).children.thickness = 1;
f.children.children(7).children.foreground = 1;
f.children.children(7).children.mark_style = 4;
f.children.children(7).children.mark_foreground = 1;
f.children.children(8).children.thickness = 1;
f.children.children(8).children.foreground = 1;
f.children.children(8).children.mark_style = 4;
f.children.children(8).children.mark_foreground = 1;
f.children.children(9).children.thickness = 1;
f.children.children(9).children.foreground = 1;
f.children.children(9).children.mark_style = 4;
f.children.children(9).children.mark_foreground = 1; //Polyline features (Dicke der Linien)
f.children.children(10).children.thickness = 1;
f.children.children(10).children.foreground = 1;
f.children.children(10).children.mark_style = 4;
f.children.children(10).children.mark_foreground = 1;
f.children.children(11).children.thickness = 3;
f.children.children(11).children.foreground = 1;
f.children.children(11).children.mark_style = 4;
f.children.children(11).children.mark_foreground = 1;
f.children.children(12).children.thickness = 3;
f.children.children(12).children.foreground = 1;
f.children.children(12).children.mark_style = 4;
f.children.children(12).children.mark_foreground = 1;
f.children.children(13).children.thickness = 3;
f.children.children(13).children.foreground = 1;
f.children.children(13).children.mark_style = 4;
f.children.children(13).children.mark_foreground = 1;
f.children.children(14).children.thickness = 3;
f.children.children(14).children.foreground = 1;
f.children.children(14).children.mark_style = 4;
f.children.children(14).children.mark_foreground = 1;
f.children.children(15).children.thickness = 3;
f.children.children(15).children.foreground = 1;
f.children.children(15).children.mark_style = 4;
f.children.children(15).children.mark_foreground = 1;
f.children.children(2).children.thickness = 3; //Polyline features (Dicke der Linien)
f.children.children(2).children.foreground = 1;
f.children.children(2).children.mark_style = 4;
f.children.children(2).children.mark_foreground = 1;
f.children.children(3).children.thickness = 3;
f.children.children(3).children.foreground = 1;
f.children.children(3).children.mark_style = 4;
f.children.children(3).children.mark_foreground = 1;
f.children.children(4).children.thickness = 3;
f.children.children(4).children.foreground = 1;
f.children.children(4).children.mark_style = 4;
f.children.children(4).children.mark_foreground = 1;


g = gce(); //Legenden Menü
g.thickness = 1;  //Dicke
g.font_size = 1;  //Schriftgröße

//Labels '$' in Latex-Schreibweise
xlabel('$\text{Zeit [min]}$', 'fontsize', 2);
ylabel('$\text{Abs. Temperatur [}^\circ\text{C]}$', 'fontsize',2);

xs2pdf(0,"graphic_output/sc_1.pdf");

