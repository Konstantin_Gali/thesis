//Verarbeitungsprogramm und Plotprogramm für 8 Sensoren
clc;clear; 

//Funktionsaufruf
exec('csv_read_conv_functions.sci')

//Korrektes Einlesen des csv-files
fileName = "KS_Spannungen_isa.csv"
[timeValues, measurementValue]=read_grafana_csv(fileName);

//Normierung Zeitwert in Sekunde(1)/Minute(60)/Stunde(3600)
t = timeValues/60;
//Einlesen von 8 y-Werten
y1 = measurementValue(:,2);
y2 = measurementValue(:,3);
y3 = measurementValue(:,4);
//y4 = measurementValue(:,5);
//y5 = measurementValue(:,6);
//y6 = measurementValue(:,7);
//y7 = measurementValue(:,8);
//y8 = measurementValue(:,9);


//Graphik - Einstellungen

scf(4); //Nummer des Graphik-Fensters
clf(); //Löschen des Graphikhintergrunds
xgrid(0); //Schwarzes Grid

//Plotten plot2d(x-Achse, y-Achse, Farbe der Polyline)
plot2d(t,y1,color("scilab blue4")); //7
plot2d(t,y2,color("scilab green4")); //16
plot2d(t,y3,color("scilab cyan4"));//5
//plot2d(t,y4,color("scilab red4")); //2
//plot2d(t,y5,color("scilab magenta4"));//3
//plot2d(t,y6,color("scilab brown4"));//4
//plot2d(t,y7,color("scilab pink4"));//1
//plot2d(t,y8,color("goldenrod"));//6

//Legende nach Auftauchen der Graphen, erster Plot = erster Legendeneintrag
legend("$\text{Zelle 1}$","$\text{Zelle 2}$","$\text{Zelle 3}$","$\text{Zelle 4}$","$\text{Zelle 5}$","$\text{Zelle 6}$","$\text{Zelle 7}$","$\text{Zelle 8}$",[2])

// Titel
//title("$\text{Zellspannungsmessung im Pack während Ladevorgang mit Balancing}$",'fontsize',6)

// Axes leyout
a = gca();
a.filled = "off"
// Layout für PNG Designs
f = gcf();
f.figure_size = [1366,768]; //For PNG Desine
f.axes_size = [1266,668]; //Graph size
f.children.font_size = 3; //Achsendicke
f.children.thickness = 1; //Grid Dicke
f.background =  -2


//Polyline features (Dicke der Linien)
f.children.children(2).children.thickness = 3;
f.children.children(3).children.thickness = 3;
f.children.children(4).children.thickness = 3;
f.children.children(5).children.thickness = 3;
f.children.children(6).children.thickness = 3;
f.children.children(7).children.thickness = 3;
f.children.children(8).children.thickness = 3;
//f.children.children(9).children.thickness = 3;

g = gce(); //Legenden Menü
g.thickness = 3;  //Dicke
g.font_size = 4;  //Schriftgröße
g.visible = "on"
g.line_mode ="off"
g.fill_mode = "on"
g.font_color = -1

//Labels '$' in Latex-Schreibweise
xlabel('$\text{Zeit in min}$', 'fontsize', 4);
ylabel('$\text{Spannung in }\text{V}$', 'fontsize',4);

//PNG Graphik Export
xs2png(4,"TemperatureMeasurement_Test_A.pdf")
