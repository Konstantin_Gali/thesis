\contentsline {chapter}{\numberline {1}Grundlagen zum \acl {bms}}{9}% 
\contentsline {section}{\numberline {1.1}Hauptaufgaben des \aclp {bms}}{9}% 
\contentsline {section}{\numberline {1.2}Charakteristiken des \aclp {bms}}{10}% 
\contentsline {subsection}{\numberline {1.2.1}\acf {cccv}}{10}% 
\contentsline {subsection}{\numberline {1.2.2}Regler}{11}% 
\contentsline {subsection}{\numberline {1.2.3}Meter}{12}% 
\contentsline {subsection}{\numberline {1.2.4}Monitor}{13}% 
\contentsline {subsection}{\numberline {1.2.5}Ausgleicher (Balancer)}{13}% 
\contentsline {subsection}{\numberline {1.2.6}Protector}{13}% 
\contentsline {subsection}{\numberline {1.2.7}Funktionalit\IeC {\"a}tsvergleich}{13}% 
\contentsline {section}{\numberline {1.3}Entwickelte Arten des \aclp {bms} }{14}% 
\contentsline {subsection}{\numberline {1.3.1}Centralized \ac {bms}}{14}% 
\contentsline {subsection}{\numberline {1.3.2}Modular \ac {bms}}{15}% 
\contentsline {subsection}{\numberline {1.3.3}Master - Slave \ac {bms}}{16}% 
\contentsline {subsection}{\numberline {1.3.4}Distributed \ac {bms}}{17}% 
\contentsline {section}{\numberline {1.4}Gesetzliche technische Normen von Ger\IeC {\"a}teentwicklung f\IeC {\"u}r Industrie und Privatgebiet }{18}% 
\contentsline {chapter}{\numberline {2}Entwicklung und Implementierung von \acl {bms}}{19}% 
\contentsline {section}{\numberline {2.1}\acl {bms} Strukturplan}{19}% 
\contentsline {section}{\numberline {2.2}Zielhardware und eingesetzte Werkzeuge}{21}% 
\contentsline {subsection}{\numberline {2.2.1}\acl {bcu}}{21}% 
\contentsline {subsection}{\numberline {2.2.2}\acl {pcu}}{22}% 
\contentsline {section}{\numberline {2.3}Implementierung und Kommunikation}{23}% 
\contentsline {subsection}{\numberline {2.3.1}\ac {isospi}}{23}% 
\contentsline {subsection}{\numberline {2.3.2}\ac {can}}{23}% 
\contentsline {subsection}{\numberline {2.3.3}Datentransfer}{24}% 
\contentsline {section}{\numberline {2.4}Spannungsmessungen}{25}% 
\contentsline {section}{\numberline {2.5}Strommessung}{27}% 
\contentsline {section}{\numberline {2.6}Temperaturmessung}{28}% 
\contentsline {section}{\numberline {2.7}Balancieren}{29}% 
\contentsline {section}{\numberline {2.8}Balancing Feedback}{31}% 
\contentsline {section}{\numberline {2.9}Temperatur Management}{33}% 
\contentsline {chapter}{\numberline {3}Ergebnis}{35}% 
\contentsline {section}{\numberline {3.1}Ergebnis Auswertung}{35}% 
