\contentsline {chapter}{\numberline {1}Einleitung}{6}
\contentsline {chapter}{\numberline {2}Theoretische Grundlagen}{7}
\contentsline {chapter}{\numberline {3}Stand der Technik}{8}
\contentsline {section}{\numberline {3.1}Energiespeicherung}{8}
\contentsline {section}{\numberline {3.2}Elektrochemische Energiespeicherung}{8}
\contentsline {section}{\numberline {3.3}Batterieskalierung}{9}
\contentsline {subsection}{\numberline {3.3.1}Zelle Module}{10}
\contentsline {subsection}{\numberline {3.3.2}Zelle System}{10}
\contentsline {section}{\numberline {3.4}Lithium Eisen Phosphat Zelle}{10}
\contentsline {subsection}{\numberline {3.4.1}Aufbau und Wirkungsweise}{10}
\contentsline {subsection}{\numberline {3.4.2}Charakteristiken}{10}
\contentsline {subsection}{\numberline {3.4.3}Batterie Einsatzgebiet}{11}
\contentsline {chapter}{\numberline {4}Arten des \acf {bms} und dessen Funktionalit\IeC {\"a}t}{12}
\contentsline {section}{\numberline {4.1}\acf {bms} Einf\IeC {\"u}hrung}{12}
\contentsline {section}{\numberline {4.2}Definition des \acfp {bms}}{13}
\contentsline {subsection}{\numberline {4.2.1}\acf {cccv}}{13}
\contentsline {subsection}{\numberline {4.2.2}Regler}{14}
\contentsline {subsection}{\numberline {4.2.3}Z\IeC {\"a}hler}{15}
\contentsline {subsection}{\numberline {4.2.4}Monitor}{16}
\contentsline {subsection}{\numberline {4.2.5}Ausgleicher (Balancer)}{16}
\contentsline {subsection}{\numberline {4.2.6}Protector}{16}
\contentsline {subsection}{\numberline {4.2.7}Funktionalit\IeC {\"a}tsvergleich}{16}
\contentsline {section}{\numberline {4.3}\ac {bms} Arten}{17}
\contentsline {subsection}{\numberline {4.3.1}Monolithisch \ac {bms}}{17}
\contentsline {subsection}{\numberline {4.3.2}Modular \ac {bms}}{18}
\contentsline {subsection}{\numberline {4.3.3}Master - Slave \ac {bms}}{19}
\contentsline {subsection}{\numberline {4.3.4}Verteilt \ac {bms}}{20}
\contentsline {section}{\numberline {4.4}Technische Normen}{20}
\contentsline {chapter}{\numberline {5}\acf {bms} Entwicklung Vorgehensweise}{22}
\contentsline {section}{\numberline {5.1}Idee der Untersuchung}{22}
\contentsline {section}{\numberline {5.2}\acf {bms} Strukturplan}{22}
\contentsline {section}{\numberline {5.3}Kommunikation}{24}
\contentsline {subsection}{\numberline {5.3.1}\ac {isospi}}{24}
\contentsline {subsection}{\numberline {5.3.2}\ac {can}}{25}
\contentsline {section}{\numberline {5.4}Zielhardware und eingesetzte Werkzeuge}{25}
\contentsline {subsection}{\numberline {5.4.1}Entwicklungsumgebung}{25}
\contentsline {subsection}{\numberline {5.4.2}Rechenmodule (\acf {bcu})}{26}
\contentsline {subsection}{\numberline {5.4.3}Implementierung}{26}
\contentsline {subsection}{\numberline {5.4.4}Datentransfer}{26}
\contentsline {section}{\numberline {5.5}Spannungsmessungen}{27}
\contentsline {section}{\numberline {5.6}Strommessung}{28}
\contentsline {section}{\numberline {5.7}Temperaturmessung}{29}
\contentsline {section}{\numberline {5.8}Balancieren}{29}
\contentsline {section}{\numberline {5.9}Balancing Feedback}{31}
\contentsline {section}{\numberline {5.10}Temperatur Management}{34}
\contentsline {chapter}{\numberline {6}Batteriesicherheitssystem Charakteristiken}{35}
\contentsline {section}{\numberline {6.1}Probleme zu Kl\IeC {\"a}ren}{35}
\contentsline {section}{\numberline {6.2}Precharge Schaltkreis}{35}
\contentsline {section}{\numberline {6.3}Strukturplan}{36}
\contentsline {section}{\numberline {6.4}Komponente}{36}
\contentsline {section}{\numberline {6.5}Aufbau}{37}
\contentsline {section}{\numberline {6.6}Kommunikation}{37}
\contentsline {section}{\numberline {6.7}Ansteuerung}{37}
\contentsline {section}{\numberline {6.8}Sicherheitsparameter state machine}{37}
\contentsline {chapter}{\numberline {7}Ergebnis}{38}
\contentsline {section}{\numberline {7.1}Ergebnis Auswertung}{38}
\contentsline {chapter}{\numberline {8}Zusammenfassung}{39}
