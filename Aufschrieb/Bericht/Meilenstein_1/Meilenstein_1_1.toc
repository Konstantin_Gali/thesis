\babel@toc {ngerman}{}
\contentsline {section}{\numberline {1}Zielsetzung der Arbeit}{3}
\contentsline {subsection}{\numberline {1.1}Batterie-Pack Monitor}{3}
\contentsline {subsection}{\numberline {1.2}Multicell Batterie Monitor LTC6811}{3}
\contentsline {subsection}{\numberline {1.3}Kommunikation}{3}
\contentsline {section}{\numberline {2}Signalfluss}{5}
\contentsline {subsection}{\numberline {2.1}Schnittstellen}{5}
\contentsline {subsection}{\numberline {2.2}Datentransfer}{5}
\contentsline {section}{\numberline {3}LTC6811 Peripherie}{6}
\contentsline {subsection}{\numberline {3.1}Spannungmessung}{6}
\contentsline {subsection}{\numberline {3.2}MUX und GPIO1}{6}
\contentsline {subsection}{\numberline {3.3}Balancing und Balancing Feedback}{7}
\contentsline {subsection}{\numberline {3.4}Temperaturmessung der Batteriezelle}{8}
