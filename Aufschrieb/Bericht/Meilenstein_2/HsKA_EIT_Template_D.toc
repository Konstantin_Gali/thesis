\contentsline {chapter}{\numberline {1}Untersuchung von Balancingwiderst\"ande}{3}
\contentsline {section}{\numberline {1.1}Allgemeine Messungen}{4}
\contentsline {subsection}{\numberline {1.1.1}Balancing Theorie}{5}
\contentsline {section}{\numberline {1.2}Widerstandsanpassung an Minimalladestrom}{5}
\contentsline {section}{\numberline {1.3}W\"armebilder von Balancing-Widerst\"ande im Batteriepack}{5}
\contentsline {subsection}{\numberline {1.3.1}Balancing-Vorgang einer Batteriezelle}{6}
\contentsline {subsection}{\numberline {1.3.2}Balancing-Vorgang alle Batteriezellen}{6}
\contentsline {section}{\numberline {1.4}Testaufbau f\"ur den Temperaturausbreitungsmessung}{7}
\contentsline {section}{\numberline {1.5}Tests}{8}
\contentsline {subsection}{\numberline {1.5.1}Test A}{9}
\contentsline {subsection}{\numberline {1.5.2}Test B}{10}
\contentsline {subsection}{\numberline {1.5.3}Test C}{11}
\contentsline {section}{\numberline {1.6}Probleme und L\"osungen}{12}
\contentsline {subsection}{\numberline {1.6.1}N\"achste Schritte}{12}
\contentsline {section}{\numberline {1.7}Fazit}{12}
\contentsline {chapter}{Literatur}{12}
