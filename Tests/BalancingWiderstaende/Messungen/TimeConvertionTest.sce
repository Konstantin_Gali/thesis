clc()
clearglobal()

datString = csvRead("TemperatureMeasurement_Opt1.csv",";",[],'string')
datDouble = csvRead("TemperatureMeasurement_Opt1.csv",";")
[n1,n2] = size(datString(:,1))
[m1,m2] = size(datDouble(:,:))

measurementTime = datString(2:n1,1)
measurementValue = datDouble(2:m1,2:m2)

timeArrayConverted = msscanf( measurementTime(1),'%d%c%d%c%d%c%d%c%d%c%d')
t0(1:6) = timeArrayConverted(1:2:11)

for i = 1:n1-1
    timeArrayConverted = msscanf( measurementTime(i),'%d%c%d%c%d%c%d%c%d%c%d')
    timeArrayInt(1:6) = timeArrayConverted(1:2:11)
    timeValues(i)=etime(timeArrayInt,t0)/3600 //Time Values for Grafix
end

t = timeValues
y1 = measurementValue(:,1);
y2 = measurementValue(:,2);
y3 = measurementValue(:,3);
y4 = measurementValue(:,4);
y5 = measurementValue(:,5);
y6 = measurementValue(:,6);
y7 = measurementValue(:,7);
y8 = measurementValue(:,8);
y9 = measurementValue(:,9);

//Plotten
scf(4);
clf();
xgrid(0);
plot2d(t,y1,color("grey"));//6
legend("$\text{NTC - Sensor 1}$")
plot2d(t,y2,color("grey")); //7
legend("$\text{NTC - Sensor 2}$")
plot2d(t,y3,color("grey")); //16
legend("$\text{NTC - Sensor 3}$")
plot2d(t,y4,5);//5
legend("$\text{NTC - Sensor 4}$")
plot2d(t,y5,color("grey"));//2
legend("$\text{NTC - Sensor 5}$")
plot2d(t,y6,color("grey"));//3
legend("$\text{NTC - Sensor 6}$")
plot2d(t,y7,color("grey"));//4
legend("$\text{NTC - Sensor 7}$")
plot2d(t,y8,1);//1
plot2d(t,y9,1);
legend("$\text{Sensor A(-0,5/-0,5)}$","$\text{Sensor A(0/-1)}$","$\text{Sensor A(-1/0)}$", ..
"$\text{Sensor A(0/0)}$","$\text{Sensor A(1/0)}$","$\text{Sensor A(0/1)}$", ..
"$\text{Sensor A(0,5/0,5)}$","$\text{Sensor A(3/0)}$")


// Layout für PNG Designs
f = gcf();
f.figure_size = [1200,1000]; //For PNG Desine
f.axes_size = [1150,950]; //Graph size
f.children.font_size = 5; //
f.children.thickness = 1;

f.children.children(2).children.thickness = 3; //Polyline features
f.children.children(3).children.thickness = 3;
f.children.children(4).children.thickness = 3;
f.children.children(5).children.thickness = 3;
f.children.children(6).children.thickness = 3;
f.children.children(7).children.thickness = 3;
f.children.children(8).children.thickness = 3;
f.children.children(9).children.thickness = 3;

g = gce(); //Legende menu
g.thickness = 3;
g.font_size = 5;

xlabel('$\text{Zeit in h}$', 'fontsize', 6);
ylabel('$\text{Temperatur in }^\circ\text{C}$', 'fontsize',6);
xs2png(4,"graph_oben1.png")
